package com.oersi.de.services;

import com.oersi.de.httprequest.HttpRequestSender;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OersiServiceImpl implements OersiService {
    private final Logger logger = LoggerFactory.getLogger(OersiServiceImpl.class);
    private String oersiUrl;
    private String eduSharingBaseUrl;

    @Override
    public void updateOersi(NodeRef nodeRef) throws InvalidNodeRefException {
        logger.info("[{}] Start Updating material with id: {} to oersi.de", this.getClass().getName(), nodeRef.getId());
        HttpRequestSender requestSender = new HttpRequestSender(new HttpClient());
        String payload = String.format("recordId=%s/components/render/%s", this.eduSharingBaseUrl, nodeRef.getId());
        String postResponse = requestSender.sendPostRequest(this.oersiUrl, payload.replaceAll("(?<!(http:|https:))/+", "/"));

        if (postResponse == null) {
            logger.error("[{}] Something went wrong, we couldn't sent the payload: {}, to Oersi.de ", this.getClass().getName(), payload);
        }
        if (postResponse != null) {
            logger.info("[{}] Successfully updated material with id: {} to oersi.de", this.getClass().getName(), nodeRef.getId());
        }
    }


    public void setOersiUrl(String oersiUrl) {
        this.oersiUrl = oersiUrl;
    }

    public void setEduSharingBaseUrl(String eduSharingBaseUrl) {
        this.eduSharingBaseUrl = eduSharingBaseUrl;
    }
}

