package com.oersi.de.services;

import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;

public interface OersiService {
    void updateOersi(NodeRef nodeRef) throws InvalidNodeRefException;
}
