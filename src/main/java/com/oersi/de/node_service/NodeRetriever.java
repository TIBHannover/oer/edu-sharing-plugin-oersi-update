package com.oersi.de.node_service;

import com.oersi.de.permission.EduSharingAspects;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;

import java.util.ArrayList;
import java.util.List;

public class NodeRetriever {


    /**
     * Get all nodes of type CCM_TYPE_IO in the specified folder, including nested folders.
     *
     * @param folderNodeRef the NodeRef of the folder
     * @return a list of NodeRefs representing all nodes of type CCM_TYPE_IO in the folder and its subfolders
     */
    public static List<NodeRef> retrieveIoContent(NodeRef folderNodeRef, NodeService nodeService) {
        List<NodeRef> result = new ArrayList<>();
        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(folderNodeRef);
        for (ChildAssociationRef childAssoc : childAssocs) {
            NodeRef childNodeRef = childAssoc.getChildRef();

            // Check if the node is of the required type CCM_TYPE_IO
            if (nodeService.getType(childNodeRef).equals(QName.createQName(EduSharingAspects.CCM_TYPE_IO))) {
                result.add(childNodeRef);
            }

            // If it's a folder, recursively search inside it for more nodes of type CCM_TYPE_IO
            if (nodeService.getType(childNodeRef).equals(ContentModel.TYPE_FOLDER)) {
                result.addAll(retrieveIoContent(childNodeRef, nodeService));
            }
        }

        return result;
    }
}
