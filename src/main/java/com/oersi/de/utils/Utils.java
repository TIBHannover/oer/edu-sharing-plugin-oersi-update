package com.oersi.de.utils;

import org.alfresco.service.namespace.QName;

import java.util.ArrayList;
import java.util.Set;

public class Utils {

    public static String[] getAspects(Set<QName> aspects) {
        if (aspects == null) {
            return null;
        }
        ArrayList<String> result = new ArrayList<>();
        for (QName aspect : aspects) {
            result.add(aspect.toString());
        }
        return result.toArray(new String[0]);
    }
}
