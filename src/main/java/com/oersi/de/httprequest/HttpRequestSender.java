package com.oersi.de.httprequest;


import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpRequestSender {
    public static final String RESPONSE_STATUS = "Success";
    private final HttpClient httpClient;
    private final Logger logger = LoggerFactory.getLogger(HttpRequestSender.class);

    public HttpRequestSender(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public String sendPostRequest(String url, String payload) {
        PostMethod postMethod = new PostMethod(url);
        postMethod.setRequestEntity(this.createStringRequestEntity(payload));

        try {
            int statusCode = httpClient.executeMethod(postMethod);

            if (statusCode == HttpStatus.SC_NO_CONTENT) {
                return HttpRequestSender.RESPONSE_STATUS;
            } else if (statusCode == HttpStatus.SC_OK) {
                String responseBody = postMethod.getResponseBodyAsString();
                return responseBody != null ? responseBody : HttpRequestSender.RESPONSE_STATUS;
            } else {
                logger.error("POST request failed for payload {}. Status code: {}, response body: {}", statusCode, payload, postMethod.getResponseBodyAsString());
            }
        } catch (Exception e) {
            logger.error("[{}] Exception occurred during POST request. {}", this.getClass().getName(), e);
        } finally {
            postMethod.releaseConnection();
        }

        return null;
    }

    private StringRequestEntity createStringRequestEntity(String payload) {
        try {
            return new StringRequestEntity(payload, "application/x-www-form-urlencoded", "UTF-8");
        } catch (Exception e) {
            logger.error("[{}] Failed to create StringRequestEntity. {}", this.getClass().getName(), e);
            return null;
        }
    }
}
