package com.oersi.de.action;

import com.oersi.de.services.OersiService;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UpdateOersiAction extends ActionExecuterAbstractBase {
    private static Logger logger = LoggerFactory.getLogger(UpdateOersiAction.class);

    /*
     * Services
     */
    private OersiService oersiService;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.alfresco.repo.action.executer.ActionExecuterAbstractBase#executeImpl
     * (org.alfresco.service.cmr.action.Action,
     * org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    protected void executeImpl(Action action, final NodeRef actionedUponNodeRef) {

        if (AuthenticationUtil.getRunAsUser() != null) {
            run(actionedUponNodeRef);
        }

        else {
            if (logger.isDebugEnabled()) {
                logger.debug("[{}] Run action as system user for node ({})'",this.getClass().getName(),actionedUponNodeRef);
            }

            AuthenticationUtil.runAsSystem(new AuthenticationUtil.RunAsWork<Void>() {
                @Override
                public Void doWork() throws Exception {
                    try {
                        run(actionedUponNodeRef);
                    }

                    catch (Exception e) {
                        logger.error("[{}] An error occurred ({})",this.getClass().getName(),e);
                    }
                    return null;
                }
            });
        }
    }

    protected void run(NodeRef actionedUponNodeRef) {
        if (actionedUponNodeRef != null) {
            try {

                oersiService.updateOersi(actionedUponNodeRef);
            }

            catch (InvalidNodeRefException inre) {
                logger.error("[{}] : NodeRef: ({}) not found. This node has changed in transaction.", this.getClass().getName(), actionedUponNodeRef.getId());
            }

            catch (net.sf.acegisecurity.AuthenticationCredentialsNotFoundException acnfe) {
                logger.error("[{}] Not credentials. ({})",this.getClass().getName(),acnfe);
            }
        }
    }


    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
        // nothing to do here
    }

    public void setOersiService(OersiService oersiService) {
        this.oersiService = oersiService;
    }
}
