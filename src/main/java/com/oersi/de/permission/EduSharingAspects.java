package com.oersi.de.permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class EduSharingAspects {
    private final Logger logger = LoggerFactory.getLogger(EduSharingAspects.class);
    public final static String CCM_ASPECT_IO_CHILDOBJECT = "{http://www.campuscontent.de/model/1.0}io_childobject";
    public final static String CCM_ASPECT_COLLECTION_IO_REFERENCE = "{http://www.campuscontent.de/model/1.0}collection_io_reference";
    public final static String CCM_TYPE_IO = "{http://www.campuscontent.de/model/1.0}io";

    /**
     * Method that check if a Node is a child
     *
     * @param aspects aspects
     * @return boolean true | false
     */
    public boolean matchesAspectRequirements( String[] aspects) {

        if (logger.isDebugEnabled()) {
            logger.debug("Checking for aspects: aspects={}", Arrays.toString(aspects));
        }

        if (aspects.length == 0 ) {
            return false;
        }
        boolean isNotChildOfParent = !Arrays.asList(aspects).contains(CCM_ASPECT_IO_CHILDOBJECT);
        boolean isCollectionIoReference = !Arrays.asList(aspects).contains(CCM_ASPECT_COLLECTION_IO_REFERENCE);
        return isNotChildOfParent && isCollectionIoReference;
    }
}
