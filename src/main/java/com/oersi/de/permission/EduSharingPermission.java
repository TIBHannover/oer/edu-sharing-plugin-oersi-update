package com.oersi.de.permission;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class EduSharingPermission {
    private final Logger logger = LoggerFactory.getLogger(EduSharingPermission.class);
    public final static String PERMISSION_CC_PUBLISH = "CCPublish";

    private PermissionService permissionService;

    /**
     * Method that check if a Node has CCPublish permission
     *
     * @param nodeRef nodeRef
     * @return boolean true | false
     */
    public boolean hasPublicPermission(NodeRef nodeRef) {

        if (logger.isDebugEnabled()) {
            logger.debug("Checking for permission: nodeRef={}, authority={}, permission={}", nodeRef, PermissionService.ALL_AUTHORITIES, PermissionService.READ_PERMISSIONS);
        }

        if (nodeRef == null) {
            return false;
        }
        // Check current permissions:
        Set<AccessPermission> permissions = permissionService.getAllSetPermissions(nodeRef);
        for (AccessPermission perm : permissions) {
            if (perm.getAuthority().equals(PermissionService.ALL_AUTHORITIES) &&
                    perm.getAuthorityType().equals(AuthorityType.EVERYONE) &&
                    perm.getPermission().equals(EduSharingPermission.PERMISSION_CC_PUBLISH)
            ) {
                return perm.getAccessStatus().equals(AccessStatus.ALLOWED);
            }
        }
        return false;
    }

    /**
     * method hat check the CCPublish permission exist
     *
     * @param authority  String, authority
     * @param permission String permission
     * @return boolean true|false
     */
    public boolean hasCCPublishPermission(String authority, String permission) {
        if (authority.isEmpty() && permission.isEmpty()) {
            return false;
        }
        return authority.equals(PermissionService.ALL_AUTHORITIES) && permission.equals(EduSharingPermission.PERMISSION_CC_PUBLISH);
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
