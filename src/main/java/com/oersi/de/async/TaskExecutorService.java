package com.oersi.de.async;

import net.sf.acegisecurity.Authentication;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TaskExecutorService {
    protected final Logger logger = LoggerFactory.getLogger(TaskExecutorService.class);
    private final ExecutorService executorService;


    public TaskExecutorService(int threadPoolSize) {
        this.executorService = new ThreadPoolExecutor(
                threadPoolSize,
                threadPoolSize * 2,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());
    }

    // Helper method to wrap a task with authentication and security context preservation
    private Runnable wrapWithAuthentication(Runnable task) {
        final Authentication fullAuthentication = AuthenticationUtil.getFullAuthentication();
        final SecurityContext securityContext = SecurityContextHolder.getContext();

        return () -> {
            AuthenticationUtil.setFullAuthentication(fullAuthentication);
            SecurityContextHolder.setContext(securityContext);
            try {
                task.run(); // Execute the actual task
            } catch (Throwable e) {
                logger.error("[{}] Exception while executing task: {}", this.getClass().getName(), e.getMessage(), e);
            }
        };
    }

    // Submit a task to the executor service
    public void submitTask(Runnable task) {
        try {
            // Submit the task to the executor service and handle rejection gracefully
            executorService.submit(wrapWithAuthentication(task));
        } catch (Throwable e) {
            logger.error("Task submission rejected due to thread pool exhaustion: {}", e.getMessage());
            // Consider adding custom fallback handling here, if needed.
        }
    }

    //     submitTask in safe mode
// Submit a task to the executor service
    @SuppressWarnings("unused")
    public void submitTaskSafe(Runnable task) {
        try {
            synchronized (this) {
                // Submit the task to the executor service and handle rejection gracefully
                executorService.submit(wrapWithAuthentication(task));
            }
        } catch (Throwable e) {
            logger.error("Safe Task submission rejected due to thread pool exhaustion: {}", e.getMessage());
            // Consider adding custom fallback handling here, if needed.
        }
    }

    // Graceful shutdown method
    public void shutdown() {
        executorService.shutdown(); // Initiates an orderly shutdown

        try {
            // Wait for existing tasks to complete
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                logger.warn("Executor service did not terminate in the specified time. Forcing shutdown.");
                executorService.shutdownNow(); // Force shutdown if timeout elapses
            }
        } catch (InterruptedException e) {
            logger.error("TaskExecutorService shutdown interrupted. Forcing shutdown now.", e);
            executorService.shutdownNow();
            Thread.currentThread().interrupt(); // Preserve interrupt status
        }
    }

    // Forcefully shutdown thread pool in case of emergency
    @SuppressWarnings("unused")
    public void shutdownNow() {
        logger.warn("Forcing immediate shutdown of TaskExecutorService.");
        executorService.shutdownNow(); // Force immediate shutdown, canceling all ongoing tasks
    }

    // Check if the executor is already shut down
    @SuppressWarnings("unused")
    public boolean isShutdown() {
        return executorService.isShutdown();
    }

    // Check if all tasks have been completed after shutdown
    @SuppressWarnings("unused")
    public boolean isTerminated() {
        return executorService.isTerminated();
    }
}
