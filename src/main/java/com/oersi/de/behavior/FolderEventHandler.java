package com.oersi.de.behavior;

import com.oersi.de.node_service.NodeRetriever;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.security.permissions.PermissionServicePolicies;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;

public class FolderEventHandler extends AbstractDocumentEventHandler {

    public void registerEventHandlers() {
/*
        OnCopyComplete: commented out for now, since the copy folder fired onAddDocument event in the same transaction
        eventManager.bindClassBehaviour(
                CopyServicePolicies.OnCopyCompletePolicy.QNAME,
                ContentModel.TYPE_FOLDER,
                new JavaBehaviour(this, "OnCopyComplete",
                        Behaviour.NotificationFrequency.TRANSACTION_COMMIT));
        onDeleteNodePolicy for folder commented out for now, since onDeleteNodePolicy fired on onDeleteNodePolicy event in the same transaction
        eventManager.bindClassBehaviour(
                CopyServicePolicies.OnCopyCompletePolicy.QNAME,
                ContentModel.TYPE_FOLDER,
                new JavaBehaviour(this, "OnCopyComplete",
                        Behaviour.NotificationFrequency.TRANSACTION_COMMIT));
*/

        eventManager.bindClassBehaviour(
                NodeServicePolicies.OnMoveNodePolicy.QNAME,
                ContentModel.TYPE_FOLDER,
                new JavaBehaviour(this, "onMoveNode",
                        Behaviour.NotificationFrequency.TRANSACTION_COMMIT));

        // Handle document property updates:
        eventManager.bindClassBehaviour(
                PermissionServicePolicies.OnRevokeLocalPermission.QNAME,
                ContentModel.TYPE_FOLDER,
                new JavaBehaviour(this, "onRevokeFolderLocalPermission",
                        Behaviour.NotificationFrequency.TRANSACTION_COMMIT));

        eventManager.bindClassBehaviour(
                PermissionServicePolicies.OnGrantLocalPermission.QNAME,
                ContentModel.TYPE_FOLDER,
                new JavaBehaviour(this, "onGrantFolderLocalPermission",
                        Behaviour.NotificationFrequency.TRANSACTION_COMMIT));
    }

/*
    @SuppressWarnings("unused")
    public void OnCopyComplete(QName classRef, NodeRef sourceNodeRef, NodeRef targetNodeRef, boolean copyToNewNode, Map<NodeRef, NodeRef> copyMap) {
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] A folder with ref ({}) has just been copied", this.getClass().getName(), targetNodeRef);
        }
        List<NodeRef> nodes = NodeRetriever.retrieveIoContent(targetNodeRef, nodeService);
        // add/override the previous node context
        nodes.forEach(this::executeActionIfEligibleAsync);

    }
*/


    @SuppressWarnings("unused")
    public void onMoveNode(ChildAssociationRef oldChildAssocRef, ChildAssociationRef newChildAssocRef) {
        NodeRef nodeRef = newChildAssocRef.getChildRef();
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] A document with ref ({}) has just been moved", this.getClass().getName(), nodeRef);
        }
        List<NodeRef> nodes = NodeRetriever.retrieveIoContent(nodeRef, nodeService);
        nodes.forEach(this::executeActionIfEligibleAsync);

    }


    @SuppressWarnings("unused")
    public void onGrantFolderLocalPermission(NodeRef nodeRef, String authority, String permission) {
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] onGrantLocalPermissionFolder: Document with ref ({}), hast just granted {} with permission {}  ", this.getClass().getName(), nodeRef, authority, permission);
        }

//        Since the OnGrantLocalPermissionPolicy get triggers multiple times, we need to check if the has ccpublish permission is already granted
        if ((nodeRef != null || nodeService.exists(nodeRef)) && this.eduSharingPermission.hasCCPublishPermission(authority, permission)) {
            List<NodeRef> nodes = NodeRetriever.retrieveIoContent(nodeRef, nodeService);
            nodes.forEach((nodeRf) -> executeActionIfEligibleAsync(nodeRf, authority, permission));
        }

    }

    @SuppressWarnings("unused")
    public void onRevokeFolderLocalPermission(NodeRef nodeRef, String authority, String permission) {
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] OnRevokeLocalPermissionFolder: Document with ref ({}), hast just revoked {} with permission {}  ", this.getClass().getName(), nodeRef, authority, permission);
        }

//        Since the OnRevokeLocalPermissionPolicy get triggers multiple times, we need to check if the has ccpublish permission is already granted
        if ((nodeRef != null || nodeService.exists(nodeRef)) && this.eduSharingPermission.hasCCPublishPermission(authority, permission)) {
            List<NodeRef> nodes = NodeRetriever.retrieveIoContent(nodeRef, nodeService);
            nodes.forEach((nodeRf) -> executeActionIfEligibleAsync(nodeRf, authority, permission));
        }
    }

}

