package com.oersi.de.behavior;

import com.oersi.de.async.TaskExecutorService;
import com.oersi.de.permission.EduSharingAspects;
import com.oersi.de.permission.EduSharingPermission;
import com.oersi.de.utils.Utils;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class for handling document events.
 * This class provides common functionality for handling document events such as adding, deleting, and updating documents.
 */
public abstract class AbstractDocumentEventHandler {

    /**
     * The name of the action to be executed.
     */
    protected final static String ACTION_NAME = "oersi.update.action.UpdateOersiAction";

    /**
     * The logger for the class.
     */
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * The policy component for handling events.
     */
    protected BehaviourFilter policyBehaviourFilter;

    /**
     * The policy component for handling events.
     */
    protected PolicyComponent eventManager;

    /**
     * The action service for executing actions.
     */
    protected ActionService actionService;

    /**
     * The permission service for handling edu sharing permissions.
     */
    protected EduSharingPermission eduSharingPermission;

    /**
     * The node service for handling nodes.
     */
    protected NodeService nodeService;


    protected EduSharingAspects eduSharingAspects;

    /**
     * The service registry for handling services.
     */
    protected ServiceRegistry serviceRegistry;


    /**
     * The TaskExecutorService for handling background tasks.
     */
    protected TaskExecutorService taskExecutorService;

    // Setter for TaskExecutorService to inject it
    public void setTaskExecutorService(TaskExecutorService taskExecutorService) {
        this.taskExecutorService = taskExecutorService;
    }


    /**
     * Sets the policy component for handling events.
     *
     * @param policyComponent The policy component to set.
     */
    public void setPolicyComponent(PolicyComponent policyComponent) {
        this.eventManager = policyComponent;
    }

    /**
     * Sets the action service for executing actions.
     *
     * @param actionService The action service to set.
     */
    public void setActionService(ActionService actionService) {
        this.actionService = actionService;
    }


    /**
     * Sets the permission service for handling edu sharing permissions.
     *
     * @param eduSharingPermission The permission service to set.
     */
    public void setEduSharingPermission(EduSharingPermission eduSharingPermission) {
        this.eduSharingPermission = eduSharingPermission;
    }

    /**
     * Sets the node service for handling nodes.
     *
     * @param nodeService The node service to set.
     */
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setEduSharingAspects(EduSharingAspects eduSharingAspects) {
        this.eduSharingAspects = eduSharingAspects;
    }


    /**
     * Sets the service registry for handling services.
     *
     * @param serviceRegistry The service registry to set.
     */
    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }


    /**
     * Sets the policy behaviour filter.
     *
     * @param policyBehaviourFilter The policy behaviour filter to set.
     */
    public void setPolicyBehaviourFilter(BehaviourFilter policyBehaviourFilter) {
        this.policyBehaviourFilter = policyBehaviourFilter;
    }


    /**
     * Registers the event handlers.
     */
    protected abstract void registerEventHandlers();

    /**
     * Executes the action if the document is eligible.
     *
     * @param nodeRef The node reference.
     */
    protected void executeActionIfEligible(NodeRef nodeRef) {
        String[] aspects = Utils.getAspects(nodeService.getAspects(nodeRef));
        if (this.eduSharingPermission.hasPublicPermission(nodeRef) && this.eduSharingAspects.matchesAspectRequirements(aspects)) {
            actionService.executeAction(actionService.createAction(AbstractDocumentEventHandler.ACTION_NAME), nodeRef);
        }
    }


    /**
     * Executes the action if the document is eligible in the async mode.
     *
     * @param nodeRef The node reference.
     */
    @SuppressWarnings("unused")
    protected void executeActionIfEligibleAsync2(NodeRef nodeRef) {
        String[] aspects = Utils.getAspects(nodeService.getAspects(nodeRef));
        if (this.eduSharingPermission.hasPublicPermission(nodeRef) && this.eduSharingAspects.matchesAspectRequirements(aspects)) {
            try {
                // Log the action details before executing
                Action action = actionService.createAction(AbstractDocumentEventHandler.ACTION_NAME);
                actionService.executeAction(action, nodeRef);
            } catch (Exception e) {
                logger.error("[{}] executeActionIfEligibleAsync: {}: ", this.getClass().getName(), e);
            }
        }
    }

    /**
     * Executes the action if the document is eligible in the async mode.
     *
     * @param nodeRef The node reference.
     */
    protected void executeActionIfEligibleAsync(NodeRef nodeRef) {
        String[] aspects = Utils.getAspects(nodeService.getAspects(nodeRef));
        if (this.eduSharingPermission.hasPublicPermission(nodeRef) && this.eduSharingAspects.matchesAspectRequirements(aspects)) {
            taskExecutorService.submitTask(() -> actionService.executeAction(actionService.createAction(AbstractDocumentEventHandler.ACTION_NAME), nodeRef));
        }
    }

    /**
     * Executes the action if the document is eligible.
     *
     * @param nodeRef The node reference.
     */
    @SuppressWarnings("unused")
    protected void executeActionIfEligible(NodeRef nodeRef, String authority, String permission) {
        String[] aspects = Utils.getAspects(nodeService.getAspects(nodeRef));
        if (this.eduSharingPermission.hasCCPublishPermission(authority, permission) && this.eduSharingAspects.matchesAspectRequirements(aspects)) {
            actionService.executeAction(actionService.createAction(AbstractDocumentEventHandler.ACTION_NAME), nodeRef);
        }
    }


    /**
     * Executes the action if the document is eligible in the async mode.
     *
     * @param nodeRef The node reference.
     */
    protected void executeActionIfEligibleAsync(NodeRef nodeRef, String authority, String permission) {
        String[] aspects = Utils.getAspects(nodeService.getAspects(nodeRef));
        if (this.eduSharingPermission.hasCCPublishPermission(authority, permission) && this.eduSharingAspects.matchesAspectRequirements(aspects)) {
            taskExecutorService.submitTask(() -> actionService.executeAction(actionService.createAction(AbstractDocumentEventHandler.ACTION_NAME), nodeRef));
        }

    }

}
