package com.oersi.de.behavior;

import com.oersi.de.permission.EduSharingAspects;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.security.permissions.PermissionServicePolicies;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.Map;

@SuppressWarnings("unused")
public class DocumentEventHandler extends AbstractDocumentEventHandler {

    public void registerEventHandlers() {
        eventManager.bindClassBehaviour(
                NodeServicePolicies.OnCreateNodePolicy.QNAME,
                QName.createQName(EduSharingAspects.CCM_TYPE_IO),
                new JavaBehaviour(this, "onAddDocument",
                        Behaviour.NotificationFrequency.TRANSACTION_COMMIT));

        eventManager.bindClassBehaviour(
                NodeServicePolicies.OnUpdateNodePolicy.QNAME,
                QName.createQName(EduSharingAspects.CCM_TYPE_IO),
                new JavaBehaviour(this, "onUpdateDocument",
                        Behaviour.NotificationFrequency.TRANSACTION_COMMIT));

        eventManager.bindClassBehaviour(
                NodeServicePolicies.BeforeDeleteNodePolicy.QNAME,
                QName.createQName(EduSharingAspects.CCM_TYPE_IO),
                new JavaBehaviour(this, "beforeDeleteNodePolicy",
                        Behaviour.NotificationFrequency.FIRST_EVENT));
        // Handle document property updates:
        eventManager.bindClassBehaviour(
                PermissionServicePolicies.OnRevokeLocalPermission.QNAME,
                QName.createQName(EduSharingAspects.CCM_TYPE_IO),
                new JavaBehaviour(this, "onRevokeLocalPermission",
                        Behaviour.NotificationFrequency.TRANSACTION_COMMIT));

        eventManager.bindClassBehaviour(
                PermissionServicePolicies.OnGrantLocalPermission.QNAME,
                QName.createQName(EduSharingAspects.CCM_TYPE_IO),
                new JavaBehaviour(this, "onGrantLocalPermission",
                        Behaviour.NotificationFrequency.TRANSACTION_COMMIT));

        eventManager.bindClassBehaviour(
                NodeServicePolicies.OnRestoreNodePolicy.QNAME,
                QName.createQName(EduSharingAspects.CCM_TYPE_IO),
                new JavaBehaviour(this, "onRestoreNode",
                        Behaviour.NotificationFrequency.EVERY_EVENT));
    }

    public void onGrantLocalPermission(NodeRef nodeRef, String authority, String permission) {
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] onGrantLocalPermission: Document with ref ({}), hast just granted {} with permission {}  ",
                    this.getClass().getName(), nodeRef, authority, permission);
        }
        if (nodeRef != null && nodeService.exists(nodeRef)) {
            executeActionIfEligibleAsync(nodeRef, authority, permission);
        }
    }

    public void onRevokeLocalPermission(NodeRef nodeRef, String authority, String permission) {
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] onRevokeLocalPermission: Document with ref ({}), hast just revoked {} with permission {}  ",
                    this.getClass().getName(), nodeRef, authority, permission);
        }
        if (nodeRef != null && nodeService.exists(nodeRef)) {
            executeActionIfEligibleAsync(nodeRef, authority, permission);
        }
    }

    public void onAddDocument(ChildAssociationRef childAssocRef) {
        NodeRef parentFolderRef = childAssocRef.getParentRef();
        NodeRef nodeRef = childAssocRef.getChildRef();
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] onAddDocument: A document with ref ({}) was just created in folder ({})",
                    this.getClass().getName(), nodeRef, parentFolderRef);
        }
        // Check if node exists, might be moved, or created and deleted in same
        // transaction.
        if (nodeRef != null && nodeService.exists(nodeRef)) {
            executeActionIfEligible(nodeRef);
        }
    }

    public void onUpdateDocument(NodeRef nodeRef) {
        NodeRef parentFolderRef = serviceRegistry.getNodeService().getPrimaryParent(nodeRef).getParentRef();
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] onUpdateDocument: A document with ref ({}) was just updated in folder ({})",
                    this.getClass().getName(), nodeRef, parentFolderRef);
        }
        if (nodeRef != null || nodeService.exists(nodeRef)) {
            executeActionIfEligible(nodeRef);
        }
    }

    public void beforeDeleteNodePolicy(final NodeRef nodeRef) {
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] A document with ref ({}) was just deleted", this.getClass().getName(), nodeRef);
        }
        executeActionIfEligibleAsync(nodeRef);
    }

    public void onRestoreNode(final ChildAssociationRef childAssocRef) {
        NodeRef nodeRef = childAssocRef.getChildRef();
        if (logger.isDebugEnabled()) {
            logger.debug("[{}] A document with ref ({}) was just restored", this.getClass().getName(), nodeRef);
        }
        if (nodeRef != null && nodeService.exists(nodeRef)) {
            executeActionIfEligible(nodeRef);
        }
    }

    public void onDocumentPropertyChanged(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        if (logger.isDebugEnabled()) {
            logger.debug("Handling document property change");
        }

        if (nodeRef != null && nodeService.exists(nodeRef)) {
            executeActionIfEligible(nodeRef);
        }
    }
}