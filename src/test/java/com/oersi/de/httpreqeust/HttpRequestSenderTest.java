package com.oersi.de.httpreqeust;

import com.oersi.de.httprequest.HttpRequestSender;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class HttpRequestSenderTest {
    String url = "http://example.com";
    String payload = "payload=http://example.com/1234";
    @Mock
    private HttpClient httpClient;

    @Mock
    private PostMethod postMethod;

    private HttpRequestSender httpRequestSender;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        httpRequestSender = new HttpRequestSender(httpClient);
    }

    @Test
    public void testSendPostRequest_Success() throws Exception {
        String expectedResponse = "Success";

        when(httpClient.executeMethod(ArgumentMatchers.any(HttpMethod.class))).thenReturn(HttpStatus.SC_OK);
        when(postMethod.getResponseBodyAsString()).thenReturn(expectedResponse);

        // Perform the test
        String actualResponse = httpRequestSender.sendPostRequest(url, payload);

        // Verify the interactions and assertions
        verify(httpClient).executeMethod(ArgumentMatchers.any(HttpMethod.class));
        assertEquals(expectedResponse,actualResponse);
    }

    @Test
    public void testSendPostRequest_FailedRequest() throws Exception {
        // Mock the HTTP client behavior
        HttpMethod httpMethod = mock(HttpMethod.class);
        when(httpClient.executeMethod(ArgumentMatchers.any(HttpMethod.class))).thenReturn(HttpStatus.SC_NOT_FOUND);

        // Perform the test
        String actualResponse = httpRequestSender.sendPostRequest(url, payload);

        // Verify the interactions and assertions
        verify(httpClient).executeMethod(ArgumentMatchers.any(HttpMethod.class));
        assertNull(actualResponse);
    }
}