package com.oersi.de.permission;


import org.alfresco.repo.security.permissions.impl.AccessPermissionImpl;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.cmr.security.PermissionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EduSharingPermissionTest {


    @Mock
    private EduSharingPermission eduSharingPermission;
    @Mock
    private PermissionService permissionService;

    private NodeRef nodeRef;
    @Before
    public void setup(){
        eduSharingPermission=new EduSharingPermission();
        eduSharingPermission.setPermissionService(permissionService);
        nodeRef=new NodeRef("workspace://TestStore/test");
    }

    @Test
    public void hasPublicPermission_True(){
        when(permissionService.getAllSetPermissions(nodeRef))
                .thenReturn(
                        Set.of(
                                new AccessPermissionImpl(
                                        "CCPublish",
                                        AccessStatus.ALLOWED,
                                        "GROUP_EVERYONE",1
                                )
                        )
                );
        boolean isPublic=eduSharingPermission.hasPublicPermission(nodeRef);

        assertEquals(true,isPublic);
    }

    @Test
    public void hasPublicPermission_False(){
        when(permissionService.getAllSetPermissions(nodeRef))
                .thenReturn(
                        Set.of(
                                new AccessPermissionImpl(
                                        "Consumer",
                                        AccessStatus.ALLOWED,
                                        "GROUP_EVERYONE",1
                                )
                        )
                );
        boolean isPublic=eduSharingPermission.hasPublicPermission(nodeRef);

        assertEquals(false,isPublic);
    }

}
