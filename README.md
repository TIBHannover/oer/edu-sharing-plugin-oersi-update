# Update Oersi when ES Materials Change

This is an Alfresco SDK module used to update ES materials in Oersi whenever a material in ES changes a property, is deleted, or transitions from public to private. This ensures that dead links will not exist in Oersi.

## How to Develop

This module can be run on a Docker instance, eliminating the need to run Alfresco and Alfresco Share separately. You can add the module by yourself, making development and testing easier. Make sure you have Docker installed on your local machine.

To run the module, use the following command:

```sh
  ./run.sh build_start
```

Or on Windows:

```sh
./run.bat build_start
```

Make sure the following tasks are completed:

- Runs Alfresco Content Service (ACS)
- (Optional) Runs Alfresco Share
- Runs PostgreSQL database
- Deploys the assembled JAR module

All the services of the project are now run as Docker containers. The run script offers the following tasks:

- `build_start`: Build the entire project, recreate the ACS Docker image, start the Dockerized environment composed of ACS, Share (optional), ASS, and PostgreSQL, and tail the logs of all the containers.
- `build_start_it_supported`: Build the whole project, including dependencies required for IT execution, recreate the ACS Docker image, start the Dockerized environment composed of ACS, Share (optional), ASS, and PostgreSQL, and tail the logs of all the containers.
- `start`: Start the Dockerized environment without building the project and tail the logs of all the containers.
- `stop`: Stop the Dockerized environment.
- `purge`: Stop the Dockerized containers and delete all the persistent data (Docker volumes).
- `tail`: Tail the logs of all the containers.
- `reload_acs`: Build the ACS module, recreate the ACS Docker image, and restart the ACS container.
- `build_test`: Build the entire project, recreate the ACS Docker image, start the Dockerized environment, execute the integration tests, and stop the environment.
- `test`: Execute the integration tests (the environment must already be started).

## How DEBUG

The project is pre-configured to listen for remote debug connections. 
By default, the remote debug port for ACS is 8888. 
This configuration can be changed through the properties `<acs.debug.port>8888</acs.debug.port>`in the pom.xml file of the main project.

### IntelliJ

 - Open the IntelliJ IDEA IDE and click on Add/Edit Configurations (top right).
 - Click on the plus icon (top left) and select Remote to add a new configuration for a remote app.
 - Enter a descriptive name for your configuration.
 - In HOST and PORT fields, give `localhost` and port `8888` if you have not changed  the default port
 - click OK
 - and then run `./run.sh build_start` to build and run the module in docker, then run the debug mode in IntelliJ, that's it

> you can also use the configuration saved into the .run folder, see debug alfresco.run.xml 


